=========================
Bell Towers
=========================

Bell Towers is a port to Drupal 7 of the beautiful business one-page Boostrap-based theme from Start Bootstrap, based on the original Golden PSD freebie from Mathavan Jaya at FreebiesXpress.

Bell Towers's original features
Fully responsive
Custom collapsing navigation with active classes, smooth page scrolling, and responsive fallback stylings
Footer with social links, copyright information, and other links
LESS files included for deeper customization options
The contact form was modified from the original template, to use Drupal's site-wide contact.

<p><img alt="Bell Towers's original header" class="js-lazy-loaded qa-js-lazy-loaded" src="https://git.drupalcode.org/project/belltowers/-/raw/7.x-1.0/img/header-bg.jpg" loading="lazy"></p>

